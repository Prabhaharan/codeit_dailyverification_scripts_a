import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import junit.framework.TestListener
import java.text.SimpleDateFormat;
import java.util.Date;
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import java.text.SimpleDateFormat
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException

ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
		
		//TEST DATA
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy h:mm aa");
	    def alertsTriggredTime= CustomKeywords.'keywordsLibrary.randomdate.getDateInput'()
	    println alertsTriggredTime
		
		// NAVIGATE TO ALERT APP
		test.log(LogStatus.INFO, "Navigate to Alert App")
	    WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/AlertAppClick"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/AlertAppClick"),FailureHandling.STOP_ON_FAILURE)
        WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/AlertAppClick"), 5,FailureHandling.STOP_ON_FAILURE)		
		
		//Dropdown Select
		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/AlertAppDropDownSelect"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/AlertAppDropDownSelect"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)

		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/SelectApplication"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/SelectApplication"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		
		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/AlertComboBox"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/AlertComboBox"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)

		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/SelectCarePlan"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/SelectCarePlan"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)

		WebUI.verifyElementPresent(findTestObject("Object Repository/Application/AlertApp/SearchClick"), 5,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject("Object Repository/Application/AlertApp/SearchClick"),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		
		def AlertsDate="//*[@role='listitem']//div[contains(text(),'"+ patientID +"')]/../parent::tr//*[text()='Care Plan']/../../following-sibling::td[1]"
		WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(AlertsDate), 6, FailureHandling.OPTIONAL)
		
		//VERIFY ALERTS RECEIVED IN THE ALERT APP
		test.log(LogStatus.INFO, "Verify alert received in the alert app")
		def carePlanAlerts=WebUI.getText(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'(AlertsDate), FailureHandling.STOP_ON_FAILURE)
		println carePlanAlerts
		Date alertsReceivedTime=new SimpleDateFormat("yyyy/MM/dd h:mm aa").parse(carePlanAlerts)
	    WebUI.verifyEqual(dateFormat.parse(carePlanAlerts).after(dateFormat.parse(alertsTriggredTime)), true, FailureHandling.STOP_ON_FAILURE)
		
		WebUI.click(findTestObject("Object Repository/LoginPage/homePageVerification"),FailureHandling.STOP_ON_FAILURE)
		GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
		test.log(LogStatus.PASS,"TestCase is executed successfully")
				
			}
		}catch(StepFailedException e){
				test.log(LogStatus.FAIL, e.message)
				CustomKeywords.'reports.extentReports.takeScreenshot'(test)
				throw e
			}