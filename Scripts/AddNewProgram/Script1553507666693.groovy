import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.main.CustomKeywordDelegatingMetaClass
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus;

ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
		
		//navigate to demographics app
		test.log(LogStatus.INFO,"Demographics App Navigation")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/Demographics/DemographicsIcon'), 10, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/DemographicsIcon'), FailureHandling.STOP_ON_FAILURE)
		
		WebUI.delay(10)
		
		//Search a patient in demographics
		test.log(LogStatus.INFO,"Search a Patient in Demographics App")
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_PatientLastNameInputField'),10).clear()
		WebUI.setText(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_PatientLastNameInputField'), patientLastName, FailureHandling.STOP_ON_FAILURE)
		
		//send ID input for patient demog search
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_PatientIDinputField'), 10).clear()
		WebUI.setText(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_PatientIDinputField'), patientID, FailureHandling.STOP_ON_FAILURE)
		
		
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_searchButton'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_searchButton'), FailureHandling.STOP_ON_FAILURE)
		
		//select searched patient
		test.log(LogStatus.INFO,"Select Searched Patient")
		TestObject patientSearchObject = findTestObject('Base/commanXpath')
		patientSearchObject.findProperty('xpath').setValue("//div[text()='" + patientID+"']/following::td/div[text()='"+patientLastName+"']")
		WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)
		WebUI.delay(5)
		WebUI.click(patientSearchObject, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Applications/Demographics/SearchPatientPage/DA_SPP_selectButton'), FailureHandling.STOP_ON_FAILURE)
		
		//Navigate to view patient Page & Navigate the programwizard 
			WebUI.verifyElementPresent(findTestObject('Application/DemographicsApp/ViewPatientPage/DA_VPP_viewPatientPageText'), 6, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/DemographicsApp/ViewPatientPage/DA_VPP_EnrollmentButton'), FailureHandling.STOP_ON_FAILURE)
			WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW'), 6, FailureHandling.OPTIONAL)
			WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW'), FailureHandling.STOP_ON_FAILURE)
			WebUI.delay(5)
			WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_AddIcon'), 15, FailureHandling.STOP_ON_FAILURE)
		
			//Delete the Existing Program
			while(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_VerticalElipsisIcon'), 4, FailureHandling.OPTIONAL))
				{
							
				WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_VerticalElipsisIcon'), 10, FailureHandling.STOP_ON_FAILURE)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_VerticalElipsisIcon'), FailureHandling.STOP_ON_FAILURE)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_DeleteIcon'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(5)
				WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_DeleteProgramPopupDeleteButton'), 10, FailureHandling.STOP_ON_FAILURE)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_DeleteProgramPopupDeleteButton'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(5)
				WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_SuccesfullDeletedPopupOkButton'), 10, FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(5)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_SuccesfullDeletedPopupOkButton'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(5)
				
			}
			
		//Program Selection Default 1stvalue from dropdown 
			
			
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_AddIcon'), FailureHandling.STOP_ON_FAILURE)
				//ParentProgram_Dropdown
				WebUI.delay(2)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ParentNameDropdown'), FailureHandling.STOP_ON_FAILURE)
	
				WebUI.delay(2)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/ParentProgramFirstValue'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(2)
					if(!(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_SubProgramPlusIcon'), 4, FailureHandling.OPTIONAL))){
					WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_PogramNameDropdown'), FailureHandling.STOP_ON_FAILURE)
					//ChildProgram selection
					WebUI.delay(2)
					WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/ChildProgramFirstValue'), FailureHandling.STOP_ON_FAILURE)
	
					//Non-Simple Program-ConsentField
					if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSelectStatusDropdown'), 4, FailureHandling.OPTIONAL)){
						WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSelectStatusDropdown'), FailureHandling.STOP_ON_FAILURE)
						WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_Program_ConsentStatusFirstValue'), FailureHandling.STOP_ON_FAILURE)
						//Consenter dropdown field
						if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentPopupConsenterDropdown'), 4, FailureHandling.OPTIONAL)){
							WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentPopupConsenterDropdown'), FailureHandling.STOP_ON_FAILURE)
							WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_Program_ConsenterFirstValue'), FailureHandling.STOP_ON_FAILURE)
						}
						WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSaveFinishButton'), FailureHandling.STOP_ON_FAILURE)
					}
				}
	
				WebUI.delay(2)
	
				//Parent-ConsentField
				if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSelectStatusDropdown'), 4, FailureHandling.OPTIONAL)){
					WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSelectStatusDropdown'), FailureHandling.STOP_ON_FAILURE)
					WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_Program_ConsentStatusFirstValue'), FailureHandling.STOP_ON_FAILURE)
					//Consenter dropdown field)
					if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentPopupConsenterDropdown'), 4, FailureHandling.OPTIONAL)){
						WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentPopupConsenterDropdown'), FailureHandling.STOP_ON_FAILURE)
						WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_Program_ConsenterFirstValue'), FailureHandling.STOP_ON_FAILURE)
					}
					WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ConsentSaveFinishButton'), FailureHandling.STOP_ON_FAILURE)
				}
				//SimpleChildSelection:
				if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_SubProgramPlusIcon'), 6, FailureHandling.OPTIONAL))
				{
					//SubProgram'+Icon'
					WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_SubProgramPlusIcon'), FailureHandling.STOP_ON_FAILURE)
					//Subprogram Selection..............................
					WebUI.delay(2)
					if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/ChildSubProgram'), 10, FailureHandling.OPTIONAL))
					{
						WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/ChildSubProgram'), FailureHandling.STOP_ON_FAILURE)
						WebUI.delay(2)
					}
					}
	
				//Status Selection
				//Program Status
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_StatusDropdown'), FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(2)
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/ProgramStatusFirstvalue'), FailureHandling.STOP_ON_FAILURE)
				//verify Program status is Inactive
				if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/ProgramEndReasonEnable'), 10, FailureHandling.OPTIONAL))
				{
					WebUI.click(findTestObject('Application/BetaProgram/ProgramEndReasonDropdown'), FailureHandling.STOP_ON_FAILURE)
					WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_Program_EndreasonFirstvalue'), FailureHandling.STOP_ON_FAILURE)
				}
			
		//SavePrgram	
				WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ProgramSaveButton'), FailureHandling.STOP_ON_FAILURE)
				if(WebUI.verifyElementPresent(findTestObject('Application/EnrollmentAndCareteamWizard/ProgramWizard/EACW_PW_ProgramSavedVerification'), 10, FailureHandling.STOP_ON_FAILURE))
		{
			test.log(LogStatus.PASS, "Patient program added successfully")
		}
		
		//Close Wizard
		WebUI.click(findTestObject('Application/EnrollmentAndCareteamWizard/EACW_WizardCloseIcon'), FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		WebUI.verifyElementPresent(findTestObject('Application/DemographicsApp/ViewPatientPage/DA_VPP_viewPatientPageText'), 6, FailureHandling.OPTIONAL)
		
		
		//switch homepage
WebUI.switchToDefaultContent()
WebUI.click(findTestObject("Object Repository/LoginPage/homePageVerification"),FailureHandling.STOP_ON_FAILURE)
GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 10, FailureHandling.OPTIONAL)


		//GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
}
}catch(StepFailedException e){
	test.log(LogStatus.FAIL, e.message)
	CustomKeywords.'reports.extentReports.takeScreenshot'(test)
	throw e
}
