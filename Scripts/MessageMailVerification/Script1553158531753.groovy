import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus;
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException
ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false

		def mailSubject = CustomKeywords.'commonutils.commonkeyword.randomTextalone'()
		
		test.log(LogStatus.INFO,"Navigate to Message app")
		//navigate to message app
		WebUI.click(findTestObject('Applications/Message/MessagesIcon'),FailureHandling.STOP_ON_FAILURE)
		WebUI.switchToWindowIndex(1)
		WebUI.maximizeWindow()
		WebUI.delay(5)
		
		//new mail
		test.log(LogStatus.INFO,"Click new and send mail")
		WebUI.click(findTestObject('Object Repository/Applications/Message/HomePage/MS_HP_NewButton'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(5)
		
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/Messages/ComposeNewMailPage/MS_CNMP_ToEmailAddressInput'), 5).clear()
		WebUI.setText(findTestObject('Object Repository/Application/Messages/ComposeNewMailPage/MS_CNMP_ToEmailAddressInput'),ToMailID ,FailureHandling.STOP_ON_FAILURE)
			
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/Messages/ComposeNewMailPage/MS_CNMP_Subject'), 5).clear()
		WebUI.setText(findTestObject('Object Repository/Application/Messages/ComposeNewMailPage/MS_CNMP_Subject'),mailSubject, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		
		
		//click send button
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/Messages/ComposeNewMailPage/MS_CNMP_Sendbutton'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Application/Messages/ComposeNewMailPage/MS_CNMP_Sendbutton'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(4)
		WebUI.verifyElementPresent(findTestObject('Application/Messages/HomePage/MS_HP_RefreshButton'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Application/Messages/HomePage/MS_HP_RefreshButton'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(4)
		
		//Verify mail in Sender's Sent Items
		test.log(LogStatus.INFO, "Verify mail in Send item's")
		WebUI.verifyElementPresent(findTestObject('Application/Messages/HomePage/MS_HP_FolderSent'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Application/Messages/HomePage/MS_HP_FolderSent'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(4)
		
		WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[text()='"+mailSubject+"']"), 10, FailureHandling.OPTIONAL)
		
		//Verify mail in Inbox's
		test.log(LogStatus.INFO, "Verify mail in Inbox")
		WebUI.verifyElementPresent(findTestObject('Object Repository/Application/Messages/HomePage/MS_HP_FolderInbox'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Object Repository/Application/Messages/HomePage/MS_HP_FolderInbox'),FailureHandling.STOP_ON_FAILURE)
		WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[text()='"+mailSubject+"']"), 10, FailureHandling.OPTIONAL)
		WebUI.delay(10)
		
		WebUI.verifyElementPresent(findTestObject('Application/Messages/HomePage/MS_HP_RefreshButton'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Application/Messages/HomePage/MS_HP_RefreshButton'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		
		WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[text()='"+mailSubject+"']"), 10, FailureHandling.OPTIONAL)
		
		//Log out Senders Message App window
		WebUI.verifyElementPresent(findTestObject('Application/Messages/HomePage/MS_HP_LogoutMessageappWindow'),10,FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Application/Messages/HomePage/MS_HP_LogoutMessageappWindow'),FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(5)
		WebUI.switchToWindowIndex(0)
		GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
		test.log(LogStatus.PASS,"TestCase is executed successfully")
	}

}catch(StepFailedException e){
	test.log(LogStatus.FAIL, e.message)
	throw e
}







