import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus;
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.exception.StepErrorException
ExtentTest test=GlobalVariable.testrep
try{
	if(GlobalVariable.testRun.contains("Y")){
		GlobalVariable.runStatus = false
	
def RoomNumber = CustomKeywords.'commonutils.commonkeyword.randomTextalone'()
//Navigate to Careplan App
test.log(LogStatus.INFO, "Navigate to careplan App")
WebUI.click(findTestObject('Application/CarePlanApp/AppcareplanIcon'), FailureHandling.STOP_ON_FAILURE)
WebUI.switchToWindowIndex(1)
WebUI.maximizeWindow()
WebUI.delay(8)

//Navigate to Search PatientPage
WebUI.verifyElementPresent(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientSearch'), 15, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientSearch'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(10)
WebUI.verifyElementPresent(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), 15, FailureHandling.OPTIONAL)

WebUiCommonHelper.findWebElement(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), 3).clear()
WebUI.setText(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_patientID'), patientID, FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Application/CarePlanApp/SearchPatientPage/CP_SPP_lookUP'), FailureHandling.STOP_ON_FAILURE)

//Verify Patient searched patient displayed

WebUI.delay(6)
TestObject patientSearchObject=findTestObject('Base/commanXpath')
patientSearchObject.findProperty('xpath').setValue("(//table[@class='dataVal'])[3]//td[contains(text(),'"+patientLastName+"')]")
WebUI.verifyElementPresent(patientSearchObject, 15, FailureHandling.OPTIONAL)

WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/ExistingEncounterListPage/CPA_EIP_EELP_EncounterInformation'),5, FailureHandling.OPTIONAL)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/ExistingEncounterListPage/CPA_EIP_EELP_EncounterInformation'),FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)
//Verify the Existing Encounter details
test.log(LogStatus.INFO, "Verify the Existing Encounter details")
WebUI.verifyElementPresent(findTestObject('Application/CarePlanApp/EncounterInformationPage/ExistingEncounterListPage/CPA_EIP_EELP__FirstEncounterId'), 6, FailureHandling.STOP_ON_FAILURE)
def existingEncounterID=WebUI.getText(findTestObject('Application/CarePlanApp/EncounterInformationPage/ExistingEncounterListPage/CPA_EIP_EELP__FirstEncounterId'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)
WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/ExistingEncounterListPage/CPA_EIP_EELP_EditEncounterIcon'), 6, FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/ExistingEncounterListPage/CPA_EIP_EELP_EditEncounterIcon'), FailureHandling.STOP_ON_FAILURE)

test.log(LogStatus.INFO, "Update Enconter")
if(WebUI.verifyElementPresent(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/EncounterEditPage/Room'), 10, FailureHandling.OPTIONAL))
	{
		WebUiCommonHelper.findWebElement(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/EncounterEditPage/Room'), 5).clear()
		WebUI.setText(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/EncounterEditPage/Room'), RoomNumber, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(2)
		WebUI.verifyMatch(CustomKeywords.'keywordsLibrary.CommomLibrary.carePlanElementClick'(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/EncounterEditPage/CPA_EIP_EEP_SaveButton')), "Pass", false, FailureHandling.STOP_ON_FAILURE)
		WebUI.verifyElementPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[contains(text(),'"+RoomNumber+"')]"), 6, FailureHandling.STOP_ON_FAILURE)
		
	}	
	else{
		WebUI.verifyMatch(CustomKeywords.'keywordsLibrary.CommomLibrary.carePlanElementClick'(findTestObject('Object Repository/Application/CarePlanApp/EncounterInformationPage/EncounterEditPage/CPA_EIP_EEP_SaveButton')), "Pass", false, FailureHandling.STOP_ON_FAILURE)
		WebUI.delay(3)
		WebUI.verifyElementNotPresent(CustomKeywords.'keywordsLibrary.CommomLibrary.dynamicElement'("//td[contains(text(),'"+RoomNumber+"')]"), 6, FailureHandling.STOP_ON_FAILURE)
		}
	WebUI.closeWindowIndex(1)
	WebUI.switchToDefaultContent()
	WebUI.delay(5)
	WebUI.click(findTestObject("Object Repository/LoginPage/homePageVerification"),FailureHandling.STOP_ON_FAILURE)
	GlobalVariable.runStatus=WebUI.verifyElementPresent(findTestObject("Object Repository/LoginPage/homePageVerification"), 20, FailureHandling.OPTIONAL)
	test.log(LogStatus.PASS,"TestCase is executed successfully")
	}

}catch(StepFailedException e){
	test.log(LogStatus.FAIL, e.message)
	CustomKeywords.'reports.extentReports.takeScreenshot'(test)
}