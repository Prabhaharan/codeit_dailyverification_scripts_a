import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.reporting.ReportUtil
import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.testdata.TestDataColumn
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import internal.GlobalVariable as GlobalVariable

Map<String, String> suiteProperties = new HashMap<String, String>();


suiteProperties.put('id', 'Test Suites/Model/AHI')

suiteProperties.put('name', 'AHI')

suiteProperties.put('description', '')
 

DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())



RunConfiguration.setExecutionSettingFile("C:\\Users\\qatester4\\Desktop\\Dailyprod_DD\\DailyProdDataDriven\\Reports\\Model\\AHI\\20190401_120245\\execution.properties")

TestCaseMain.beforeStart()

TestCaseMain.startTestSuite('Test Suites/Model/AHI', suiteProperties, [new TestCaseBinding('Test Cases/Login', 'Test Cases/Login',  [ 'userID' : 'prabhaharan.velu@gsihealth.com' , 'url' : 'https://ahi.gsihealth.com' , 'ENV' : 'AHI' , 'password' : 'Imax123#' ,  ]), new TestCaseBinding('Test Cases/UpdateUser', 'Test Cases/UpdateUser',  [ 'userID' : 'prabhaharan.velu@gsihealth.com' , 'userFirstName' : 'Prabhaharan' , 'userLastName' : 'Velu' ,  ]), new TestCaseBinding('Test Cases/AlertVerification', 'Test Cases/AlertVerification',  [ 'patientID' : '15750' ,  ])])
