<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_MPP_RaceDropDown</name>
   <tag></tag>
   <elementGuidId>cbd423be-d15c-47a2-a210-2a93eb008ab5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;Race&quot;]/..//following-sibling::td/table/tbody/tr/td[@class=&quot;comboBoxItemPickerCell&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;Race&quot;]/..//following-sibling::td/table/tbody/tr/td[@class=&quot;comboBoxItemPickerCell&quot;]</value>
   </webElementProperties>
</WebElementEntity>
