<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SelectAction</name>
   <tag></tag>
   <elementGuidId>5fd01631-d060-4eaa-b530-324b3651ffd2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@name='accountoption']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@name='accountoption']</value>
   </webElementProperties>
</WebElementEntity>
