<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>HMLScoringValue</name>
   <tag></tag>
   <elementGuidId>194c39b5-3496-41ef-bc7e-8e0a002333c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//font[text()='HML Scoring (Preliminary)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//font[text()='HML Scoring (Preliminary)']</value>
   </webElementProperties>
</WebElementEntity>
