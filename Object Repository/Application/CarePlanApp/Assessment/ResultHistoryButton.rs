<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ResultHistoryButton</name>
   <tag></tag>
   <elementGuidId>8eff7822-3b1a-43cc-85ee-d4b7bdceaf83</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(text(),'Results')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(text(),'Results')]</value>
   </webElementProperties>
</WebElementEntity>
