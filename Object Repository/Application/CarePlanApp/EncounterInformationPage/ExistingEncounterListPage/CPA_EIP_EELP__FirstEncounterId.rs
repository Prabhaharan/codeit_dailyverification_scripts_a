<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CPA_EIP_EELP__FirstEncounterId</name>
   <tag></tag>
   <elementGuidId>f21d986e-23a1-4a82-ba4e-3d540295bfdf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[@title='Click to View Encounter Details'][2]</value>
   </webElementProperties>
</WebElementEntity>
