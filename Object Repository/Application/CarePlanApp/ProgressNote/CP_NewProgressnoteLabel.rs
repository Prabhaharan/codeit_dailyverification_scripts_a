<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_NewProgressnoteLabel</name>
   <tag></tag>
   <elementGuidId>0de29db6-6255-4d26-8703-9429c97982ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()[normalize-space() ='Add New Individual Note']]</value>
   </webElementProperties>
</WebElementEntity>
