<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Interactive_CarePlan_Ok</name>
   <tag></tag>
   <elementGuidId>94068db2-24a0-4948-a21b-6a4452341cf4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()='Ok' or text()='OK']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()='Ok' or text()='OK']</value>
   </webElementProperties>
</WebElementEntity>
