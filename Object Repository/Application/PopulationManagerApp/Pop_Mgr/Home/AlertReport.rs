<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AlertReport</name>
   <tag></tag>
   <elementGuidId>806c959b-a98e-4730-95ae-b7cace8febcc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div/div[text()='Alerts Report'][count(. | //*[@ref_element = 'Object Repository/Applications/PopulationManagerApp/Pop_Mgr/Home/iframe_browser.perspective']) = count(//*[@ref_element = 'Object Repository/Applications/PopulationManagerApp/Pop_Mgr/Home/iframe_browser.perspective'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ext</name>
      <type>Main</type>
      <value>AlertsReport.prpt</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>path</name>
      <type>Main</type>
      <value>/public/Standard Operational Reports/AlertsReport.prpt</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div/div[text()='Alerts Report']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>08eb7340-c227-48bf-b947-79e012b9d2ed</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>//div/div[text()='Alerts Report']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Alerts Report</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>//div/div[text()='Alerts Report']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>desc</name>
      <type>Main</type>
      <value>Alerts Report</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Application/PopulationManagerApp/Pop_Mgr/Home/iframe_browser.perspective</value>
   </webElementProperties>
</WebElementEntity>
