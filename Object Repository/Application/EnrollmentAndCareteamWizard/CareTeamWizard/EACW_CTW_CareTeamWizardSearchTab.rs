<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_CTW_CareTeamWizardSearchTab</name>
   <tag></tag>
   <elementGuidId>585112cc-8027-43fd-88ef-88e18576a28c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),&quot;Choose a New Team&quot;)]//following-sibling::input[@type=&quot;search&quot;]</value>
   </webElementProperties>
</WebElementEntity>
