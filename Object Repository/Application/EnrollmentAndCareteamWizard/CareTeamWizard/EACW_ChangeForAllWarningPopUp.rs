<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_ChangeForAllWarningPopUp</name>
   <tag></tag>
   <elementGuidId>3f24c518-e17e-4d44-acf2-67ac40661abd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[text()='Warning!']/following::div[text()='This Care Team is shared by other patients.']</value>
   </webElementProperties>
</WebElementEntity>
