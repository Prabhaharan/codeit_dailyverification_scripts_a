<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PatientMessagingCheckbox</name>
   <tag></tag>
   <elementGuidId>bb8a4dee-af77-4f46-95bc-dc647c97c36a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class=&quot;md-container md-ink-ripple&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@class=&quot;md-container md-ink-ripple&quot;]</value>
   </webElementProperties>
</WebElementEntity>
