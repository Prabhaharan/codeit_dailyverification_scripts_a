<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_WizardCloseIcon</name>
   <tag></tag>
   <elementGuidId>f2e44047-63c9-43c0-a8f8-07e77805920f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[text()='Enrollment &amp; Care Team Management']/parent::div//*[@md-svg-icon='close']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[text()='Enrollment &amp; Care Team Management']/parent::div//*[@md-svg-icon='close']</value>
   </webElementProperties>
</WebElementEntity>
