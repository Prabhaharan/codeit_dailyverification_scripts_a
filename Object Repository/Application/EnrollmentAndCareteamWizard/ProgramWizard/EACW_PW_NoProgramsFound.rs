<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_NoProgramsFound</name>
   <tag></tag>
   <elementGuidId>9b930590-86ea-41a2-ba4e-e7e10bf20b9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[normalize-space(text())='No programs found.']</value>
   </webElementProperties>
</WebElementEntity>
