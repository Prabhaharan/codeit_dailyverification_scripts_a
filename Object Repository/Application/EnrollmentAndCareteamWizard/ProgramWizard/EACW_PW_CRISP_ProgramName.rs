<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_CRISP_ProgramName</name>
   <tag></tag>
   <elementGuidId>add5281f-f770-4db3-b1b6-0f20ad29e0e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-option/div[normalize-space(text()) = 'Maternal and Infant Home Visiting']</value>
   </webElementProperties>
</WebElementEntity>
