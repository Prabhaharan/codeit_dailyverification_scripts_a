<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_ErrorOccurredDeletingProgram</name>
   <tag></tag>
   <elementGuidId>7c33eecf-0d31-4574-a84a-a2bc966b0ce4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='An error occurred while deleting a program.']</value>
   </webElementProperties>
</WebElementEntity>
