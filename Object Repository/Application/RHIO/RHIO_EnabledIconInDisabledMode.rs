<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RHIO_EnabledIconInDisabledMode</name>
   <tag></tag>
   <elementGuidId>5c8cd2c1-c18a-4344-b581-a0f01dbf1e20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-switch[(@name=&quot;sharing_switch&quot;)  and (@aria-checked=&quot;false&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
