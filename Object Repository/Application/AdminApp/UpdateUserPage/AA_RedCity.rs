<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_RedCity</name>
   <tag></tag>
   <elementGuidId>806e937c-6b16-4067-9bf5-653baacf3926</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'City')]//parent::nobr/..//following-sibling::td//following::input[@name=&quot;isc_TextItem_15&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'City')]//parent::nobr/..//following-sibling::td//following::input[@name=&quot;isc_TextItem_15&quot;]</value>
   </webElementProperties>
</WebElementEntity>
