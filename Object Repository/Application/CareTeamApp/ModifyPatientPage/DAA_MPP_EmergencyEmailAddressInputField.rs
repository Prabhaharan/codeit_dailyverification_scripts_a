<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DAA_MPP_EmergencyEmailAddressInputField</name>
   <tag></tag>
   <elementGuidId>fcdfe6fa-9e05-4567-98f1-98895493b340</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'emergencyEmailTextItem']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>emergencyEmailTextItem</value>
   </webElementProperties>
</WebElementEntity>
