<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddToPatientPanelButton</name>
   <tag></tag>
   <elementGuidId>a9179c68-40ca-4918-ae0b-4b92078c2fb6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[text()='Add To Member Panel'] | //div[text()='Add To Patient Panel']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()='Add To Member Panel'] | //div[text()='Add To Patient Panel']</value>
   </webElementProperties>
</WebElementEntity>
