<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>SelectCarePlan</name>
   <tag></tag>
   <elementGuidId>b699fea6-ca85-4dfc-a551-7be4c2753a70</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tr[@role='option']//div[text()='Care Plan']</value>
   </webElementProperties>
</WebElementEntity>
