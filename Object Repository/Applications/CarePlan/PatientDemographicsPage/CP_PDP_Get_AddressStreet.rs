<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CP_PDP_Get_AddressStreet</name>
   <tag></tag>
   <elementGuidId>fc78b93b-28ce-4246-962f-1934749b2185</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[contains(text(),'Address Street:')]//following::table[@class='dataVal']//td[normalize-space(text())]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),'Address Street:')]//following::table[@class='dataVal']//td[normalize-space(text())]</value>
   </webElementProperties>
</WebElementEntity>
