package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.google.common.util.concurrent.AbstractCatchingFuture.CatchingFuture
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.relevantcodes.extentreports.ExtentTest
import com.sun.java.util.jar.pack.Instruction.Switch

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import keywordsLibrary.CommomLibrary
import reports.ReportFile

public class CareBookApp {
	CommomLibrary commonLibrary;
	ReportFile reportFile

	@Keyword
	public Boolean careBookSearchField(String patientFirstName, String patientLastName){
		try{
			if(!patientFirstName.equals("")){
				WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientFirstNameInputField'), 5).clear()
				WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientFirstNameInputField'), patientFirstName, FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(1)
			}
			if(!patientLastName.equals("")){
				WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'), 5).clear()
				WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'),patientLastName, FailureHandling.STOP_ON_FAILURE)
				WebUI.delay(3)
			}
			return true
		}catch(Exception e){
			return false
		}
	}


	//	@Keyword
	//	public Boolean patientSearch(ExtentTest test,String searchCombination,String patientID,String FirstName, String LastName){
	//		Boolean verifyStatus=false
	//		def printStatement
	//
	//		try{
	//			//	WebUI.verifyMatch(CustomKeywords.'reports.Reports.print'(test,verifyStatus,printStatemnet), "Pass", false, FailureHandling.STOP_ON_FAILURE)
	//			switch(searchCombination){
	//				case "ValidFNLN":
	//					printStatement="Search Patient with valid FirstName and Valid LastName in Care Book Search section"
	//					WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientFirstNameInputField'), 5).clear()
	//					WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientFirstNameInputField'), patientFirstName, FailureHandling.STOP_ON_FAILURE)
	//					WebUI.delay(1)
	//					WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'), 5).clear()
	//					WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'),patientLastName, FailureHandling.STOP_ON_FAILURE)
	//					WebUI.delay(3)
	//					WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_searchIcon'), FailureHandling.STOP_ON_FAILURE)
	//					def patientObjectInSearchListWindow ="//div/h2[text()='Carebook Search']/../following::*/div[normalize-space(text())='"+patientLastName+"']/following::div[normalize-space(text())='"+patientID+"']"
	//					verifyStatus=WebUI.verifyElementPresent(commonLibrary.dynamicElement(patientObjectInSearchListWindow), 10, FailureHandling.OPTIONAL)
	//					if(verifyStatus){
	//						WebUI.doubleClick(commonLibrary.dynamicElement(patientObjectInSearchListWindow), FailureHandling.STOP_ON_FAILURE)
	//						verifyStatus = true
	//					}else{
	//						if(WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), 3, FailureHandling.OPTIONAL)){
	//							WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), FailureHandling.STOP_ON_FAILURE)
	//						}
	//					}
	//					break
	//				case "ValidLN":
	//					printStatement="Search Patient with Valid LastName in Care Book Search section"
	//					WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'), 5).clear()
	//					WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'),patientLastName, FailureHandling.STOP_ON_FAILURE)
	//					WebUI.delay(3)
	//					WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_searchIcon'), FailureHandling.STOP_ON_FAILURE)
	//					def patientObjectInSearchListWindow ="//div/h2[text()='Carebook Search']/../following::*/div[normalize-space(text())='"+patientLastName+"']/following::div[normalize-space(text())='"+patientID+"']"
	//					verifyStatus=WebUI.verifyElementPresent(commonLibrary.dynamicElement(patientObjectInSearchListWindow), 10, FailureHandling.OPTIONAL)
	//					if(verifyStatus){
	//						WebUI.doubleClick(commonLibrary.dynamicElement(patientObjectInSearchListWindow), FailureHandling.STOP_ON_FAILURE)
	//						verifyStatus = true
	//					}else{
	//						if(WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), 3, FailureHandling.OPTIONAL)){
	//							WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), FailureHandling.STOP_ON_FAILURE)
	//						}
	//					}
	//
	//					break
	//				case {"ValidFN" || "InValidFN" || "SpecialCharacterFN"}:
	//					if(searchCombination.equals("ValidFN")){
	//						printStatement="Search Patient with Valid FirstName in Care Book Search section"
	//					}else if(searchCombination.equals("InValidFN")){
	//						printStatement="Search Patient with InValid FirstName in Care Book Search section"
	//					}else{
	//						printStatement="Search Patient with FirstName as special character in Care Book Search section"
	//					}
	//					WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientFirstNameInputField'), 5).clear()
	//					WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientFirstNameInputField'), patientFirstName, FailureHandling.STOP_ON_FAILURE)
	//					WebUI.delay(1)
	//					WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_searchIcon'), FailureHandling.STOP_ON_FAILURE)
	//					WebUI.delay(4)
	//					verifyStatus=WebUI.verifyElementNotPresent(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), 10, FailureHandling.OPTIONAL)
	//					if(WebUI.verifyElementPresent(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), 3, FailureHandling.OPTIONAL)){
	//						WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), FailureHandling.STOP_ON_FAILURE)
	//					}
	//					break
	//				case {"InValidFNLN" || "SpecialCharacterFNLN"}:
	//					if(searchCombination.equals("InValidFNLN")){
	//						printStatement="Search Patient with Invalid FirstName and LastName in Care Book Search section"
	//					}else{
	//						printStatement="Search Patient with FirstName and LastName as special character in Care Book Search section"
	//					}
	//					WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientFirstNameInputField'), 5).clear()
	//					WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientFirstNameInputField'), patientFirstName, FailureHandling.STOP_ON_FAILURE)
	//					WebUI.delay(1)
	//					WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'), 5).clear()
	//					WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'),patientLastName, FailureHandling.STOP_ON_FAILURE)
	//					WebUI.delay(3)
	//					WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_searchIcon'), FailureHandling.STOP_ON_FAILURE)
	//					verifyStatus=WebUI.verifyElementNotPresent(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupNoMatchingText'), 10, FailureHandling.OPTIONAL)
	//					WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), FailureHandling.STOP_ON_FAILURE)
	//					verifyStatus = true
	//					break
	//				case {"InValidLN" || "SpecialCharacterLN"}:
	//					if(searchCombination.equals("InValidFNLN")){
	//						printStatement="Search Patient with Invalid LastName in Care Book Search section"
	//					}else{
	//						printStatement="Search Patient with LastName as special character in Care Book Search section"
	//					}
	//					WebUiCommonHelper.findWebElement(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'), 5).clear()
	//					WebUI.setText(findTestObject('Applications/CareBookApp/SearchPatientField/CBA_SPF_PatientLastNameInputField'),patientLastName, FailureHandling.STOP_ON_FAILURE)
	//					WebUI.delay(3)
	//					WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_searchIcon'), FailureHandling.STOP_ON_FAILURE)
	//					verifyStatus=WebUI.verifyElementNotPresent(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupNoMatchingText'), 10, FailureHandling.OPTIONAL)
	//					WebUI.click(findTestObject('Object Repository/Applications/CareBookApp/SearchPatientField/CBA_SPF_SearchPopupCancelIcon'), FailureHandling.STOP_ON_FAILURE)
	//					verifyStatus = true
	//					break
	//			}
	//		}catch(Exception e){
	//			printStatement= "Exception occurred -> "+printStatement+" => "+e
	//			verifyStatus = false
	//		} finally{
	//			WebUI.verifyMatch(reportFile.print(test, verifyStatus, printStatement), "Pass", false, FailureHandling.OPTIONAL)
	//			return verifyStatus
	//		}
	//	}
}
