package application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import keywordsLibrary.CommomLibrary
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper

//import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI


public class PreferncesKeyword {
	@Keyword
	public Boolean PrefernecesNavigation(){
		try{
			WebUI.verifyElementPresent(findTestObject('Application/Preferences/DropDownMenuClick'), 5,FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/Preferences/DropDownMenuClick'),FailureHandling.STOP_ON_FAILURE)
			Boolean dropDownStatus=WebUI.verifyElementPresent(findTestObject('Application/Preferences/DropDownMenuClick'), 5,FailureHandling.STOP_ON_FAILURE)
			return dropDownStatus
		}
		catch(Exception e){
			return false
		}
	}
	@Keyword
	public Boolean PreferenceLink(){
		try{
			WebUI.verifyElementPresent(findTestObject('Application/Preferences/DropDownPreferenceLink'), 5,FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('Application/Preferences/DropDownPreferenceLink'),FailureHandling.STOP_ON_FAILURE)
			Boolean PreferenceStatus=WebUI.verifyElementPresent(findTestObject('Application/Preferences/DropDownPreferenceLink'), 5,FailureHandling.STOP_ON_FAILURE)
			return PreferenceStatus
		}
		catch(Exception e){
			return false
		}
	}
}
