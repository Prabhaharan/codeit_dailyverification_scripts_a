package runSelection

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class testcaseExecution {
	
	Map<String,String> runSelect = new HashMap<String,String>()
	List<String> runList = new ArrayList()
	List<String> selectedReg = new ArrayList()
	
	
	
	
	@Keyword
	public String testcaseRun(String testcaseRunName,String testSuiteRunName){
		println testcaseRunName
		def regType = TestDataFactory.findTestData("Data Files/DailyProdData/clientSelection")
		def testcaseSelect = TestDataFactory.findTestData("Data Files/DailyProdData/testcaseSelection")
		if(regType.hasHeaders().equals(true)){
			for(def regtypeSelect:regType.columnNames){
					def selectedregType =regType.getValue(regtypeSelect, regType.rowNumbers)
					if(selectedregType.equalsIgnoreCase("y")||selectedregType.equalsIgnoreCase("yes")){
						selectedReg.add(regtypeSelect)
					}
				}
		}
		
		println selectedReg
		def testcaserowIndex
		for(def regName:selectedReg){
			if(testcaseSelect.columnNames.contains(regName)){
				  def rowCount = testcaseSelect.getRowNumbers()
				  for(int rowIndex=1;rowIndex<=rowCount;rowIndex++){
					def testcaseName =testcaseSelect.getValue("TestCaseName", rowIndex)
					def testcaseNameIndexValue =testcaseSelect.getValue(1,rowIndex)
				
					if(testcaseName.contains(testcaseRunName)||testcaseNameIndexValue.contains(testcaseRunName)){
						println testcaseRunName
						println rowIndex
						testcaserowIndex = rowIndex
					}
				  }
				 def runValue = testcaseSelect.getValue(testSuiteRunName,testcaserowIndex)
				
				 if(runValue.equalsIgnoreCase("y")||runValue.equalsIgnoreCase("yes")){
					 runSelect.put(regName,runValue)
					 runList.add(runValue)
				 }
			}
		}
		return runList
	}

}
